//
//  AppDelegate.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: - Properties
    
    var window: UIWindow?
    var appCoordinator: AppCoordinator?

    // MARK: - Delegate lifecycle
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if let window = window {
            let navigationController = UINavigationController()
            appCoordinator = AppCoordinator(with: (navigationController, window))
            appCoordinator?.start()
        }
        
        return true
    }
}

