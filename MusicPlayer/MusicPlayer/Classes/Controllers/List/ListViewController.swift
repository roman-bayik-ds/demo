//
//  ListViewController.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - ListViewController
class ListViewController: UIViewController {
	
	// MARK: - Properties
	
	var presenter: Presenter!
	
	// MARK: - Outlets
	
	@IBOutlet private weak var songImageContainerView: UIView!
	@IBOutlet private weak var songImageView: UIImageView!
	@IBOutlet private weak var collectionView: UICollectionView! {
		didSet {
			
			collectionView.dataSource = self
			collectionView.delegate = self
			collectionView.registerCell(xib: SongCollectionViewCell.self)
		}
	}
	
	
	@IBOutlet private weak var settingsButtonView: AppButtonView! {
		didSet {
			
			settingsButtonView.setType(type: .settings)
			settingsButtonView.delegate = self
		}
	}
	
	@IBOutlet private weak var likeButtonView: AppButtonView! {
		didSet {
			
			likeButtonView.setType(type: .like)
			likeButtonView.delegate = self
		}
	}
	
	
	// MARK: - Controller lifecycle
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		presenter.onViewDidLoad()
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		songImageView.roundByWidth()
		songImageContainerView.removeShadows()
		songImageContainerView.addShadows(cornerRadius: songImageContainerView.frame.width / 2)
	}
}

// MARK: - UICollectionViewDataSource

extension ListViewController: UICollectionViewDataSource {
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		presenter.numberOfItems
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let cell: SongCollectionViewCell = collectionView.instiateCell(for: indexPath)
		
		cell.delegate = self
		
		presenter.configureCell(cell, at: indexPath)
		
		return cell
	}
}


// MARK: - UICollectionViewDelegate

extension ListViewController: UICollectionViewDelegate {
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		
		presenter.didSelectItem(at: indexPath)
	}
}

// MARK: - UICollectionViewDelegateFlowLayout

extension ListViewController: UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						sizeForItemAt indexPath: IndexPath) -> CGSize {
		
		let height = view.frame.size.height * 81/896
		let width = collectionView.frame.width
		
		return CGSize(width: width , height: height)
		//presenter.sizeForItem(at: indexPath, in: collectionView)
	}
	
	func collectionView(_ collectionView: UICollectionView,
						layout collectionViewLayout: UICollectionViewLayout,
						referenceSizeForHeaderInSection section: Int) -> CGSize {
		presenter.sizeForHeader
	}
}

// MARK: - ViewProtocol
extension  ListViewController: ViewProtocol {
	typealias Presenter = ListPresenterProtocol
}

// MARK: - ListViewProtocol
extension ListViewController: ListViewProtocol {
	
	func reloadCollectionViewItems(at indexPaths: [IndexPath]) {
		collectionView.reloadItems(at: indexPaths)
	}
	
	func updateSongImageView(image: UIImage?) {
		songImageView.image = image
	}
}

// MARK: - SongCollectionViewCellDelegate
extension ListViewController: SongCollectionViewCellDelegate {
	
	func didPressPlayButton(at cell: SongCollectionViewCell) {
		
		guard let indexPath = collectionView.indexPath(for: cell) else { return }
		presenter.didPressPlayButton(at: cell, with: indexPath)
	}
}

// MARK: - AppButtonViewDelegate
extension ListViewController: AppButtonViewDelegate {
	
	func appButtonTouchedUp(_ purpleButtonView: AppButtonView, type: AppButtonType) {
		switch type {

		case .like:
			presenter.onLikeButtonClicked()
		case .settings:
			presenter.onSettingsButtonClicked()
		default:
			break
		}
	}
}
