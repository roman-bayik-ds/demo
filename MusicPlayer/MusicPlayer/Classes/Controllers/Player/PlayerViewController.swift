//
//  PlayerViewController.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - PlayerViewController
class PlayerViewController: UIViewController {
    
    // MARK: - Properties
    
    var presenter: Presenter!
    
    // MARK: - Outlets
	
	@IBOutlet weak var backwardButtonView: AppButtonView! {
		didSet {
			
			backwardButtonView.delegate = self
			backwardButtonView.setType(type: .backward)
		}
	}
	
	@IBOutlet weak var forwardButtonView: AppButtonView! {
		didSet {
			
			forwardButtonView.delegate = self
			forwardButtonView.setType(type: .forward)
		}
	}
	
	@IBOutlet weak var playButtonView: AppButtonView! {
		didSet {
			
			playButtonView.delegate = self
			playButtonView.setType(type: .play)
		}
	}
	
	@IBOutlet weak var listButtonView: AppButtonView! {
		didSet {
			
			listButtonView.delegate = self
			listButtonView.setType(type: .list)
		}
	}
	
	@IBOutlet private weak var songImageContainerView: UIView!
    @IBOutlet private weak var songImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var artistLabel: UILabel!
    @IBOutlet private weak var progressTimeLabel: UILabel!
    @IBOutlet private weak var durationLabel: UILabel!
    @IBOutlet private weak var slider: SliderView! {
        didSet {
            slider.delegate = self
        }
    }
    
    // MARK: - Controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.onViewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        songImageView.roundByWidth()
        songImageContainerView.removeShadows()
        songImageContainerView.addShadows(cornerRadius: songImageContainerView.frame.width / 2)
    }
}

// MARK: - PlayerViewProtocol
extension PlayerViewController: ViewProtocol, PlayerViewProtocol {

    typealias Presenter = PlayerPresenterProtocol
    
    func set(songImage: UIImage?,
             title: String?,
             artist: String?,
             duration: String?,
             isSelectedPlayButton: Bool,
             playButtonImage: UIImage?,
             isForwardButtonEnabled: Bool,
             isBackwardButtonEnabled: Bool) {
        
        songImageView.image = songImage
        titleLabel.text = title
        artistLabel.text = artist
        durationLabel.text = duration
		playButtonView.setButtonIsSelected(isSelectedPlayButton)
		playButtonView.setButtonImage(playButtonImage)
		playButtonView.changeShadowOffsets(isSelectedPlayButton)
		forwardButtonView.setButtonIsEnabled(isForwardButtonEnabled)
		backwardButtonView.setButtonIsEnabled(isBackwardButtonEnabled)
    }
    
    func updateSliderProgress(value: Float) {
		
		guard let slider = slider else {
			return
		}
		
        slider.setProgress(value: CGFloat(value))
    }
    
    func updatePlayerCurrentTime(currentTime: String?) {
		
        guard let progressTimeLabel = progressTimeLabel else { return }
        progressTimeLabel.text = currentTime
    }
	
	func setPlayButtonUI(image: UIImage?, isPlaying: Bool) {
		
		playButtonView.setButtonImage(image)
		playButtonView.setButtonIsSelected(isPlaying)
		playButtonView.changeShadowOffsets(isPlaying)
	}
}

// MARK: - SliderViewDelegate
extension PlayerViewController: SliderViewDelegate {
	
    func sliderViewValueDidChanged(_ value: CGFloat) {
        presenter.onSliderValueChanged(value)
    }
}

// MARK: - PlayerViewController
extension PlayerViewController: AppButtonViewDelegate {
	
	func appButtonTouchedUp(_ purpleButtonView: AppButtonView, type: AppButtonType) {
		
		switch type {
		
		case .backward:
			presenter.onBackwardButtonTouchedUp()
		case .forward:
			presenter.onForwardTouchedUp()
		case .play:
			presenter.onPlayButtonTouchedUp()
		case .list:
			presenter.onListButtonPressed()
		default:
			break
		}
	}
}
