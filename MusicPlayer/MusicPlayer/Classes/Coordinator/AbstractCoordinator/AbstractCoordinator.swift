//
//  AbstractCoordinator.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - AbstractCoordinator
class AbstractCoordinator: NSObject, AbstractCoordinatorProtocol {

	// MARK: - Properties
	
    private(set) var children: [AbstractCoordinatorProtocol] = []
    private(set) var navigation: UINavigationController = UINavigationController()

    // MARK: - Initialization
    
    init(with config: UINavigationController) {
        self.navigation = config
    }

    // MARK: - Public methods
    
    func addChild(_ coordinator: AbstractCoordinatorProtocol) {
        children.append(coordinator)
    }

    func removeChild(_ coordinator: AbstractCoordinatorProtocol) {
        
		for (index, element) in children.enumerated() {
            
			if element === coordinator {
				
                children.remove(at: index)
                break
            }
        }
    }
}
