//
//  AbstractCoordinatorProtocol.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - AbstractCoordinatorProtocol
protocol AbstractCoordinatorProtocol: AnyObject {
    
    var children: [AbstractCoordinatorProtocol] { get }
    var navigation: UINavigationController { get }
    
    func addChild(_ coordinator: AbstractCoordinatorProtocol)
    func removeChild(_ coordinator: AbstractCoordinatorProtocol)
}
