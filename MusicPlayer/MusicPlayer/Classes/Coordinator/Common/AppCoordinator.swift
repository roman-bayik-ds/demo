//
//  AppCoordinator.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - AppCoordinator
final class AppCoordinator: AbstractCoordinator, CoordinatorProtocol {

    typealias Config = (navigation: UINavigationController, window: UIWindow)
    typealias Parameters = Void

    // MARK: - Properties

    private let window: UIWindow

    // MARK: - Init methods

    init(with config: Config) {
		
        self.window = config.window
        super.init(with: config.navigation)
        window.rootViewController = navigation
        window.makeKeyAndVisible()
    }
    
    // MARK: - Public methods
    
    func start(with parameters: Parameters = ()) {
        
        let playerCoordinator = PlayerCoordinator(with: navigation)
        
        addChild(playerCoordinator)
        
        playerCoordinator.start()
    }
}
