//
//  CoordinatorProtocol.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import Foundation

// MARK: - CoordinatorProtocol
protocol CoordinatorProtocol: AbstractCoordinatorProtocol {
    
    associatedtype Config
    associatedtype Parameters
    
	// MARK: - Initialization
	
	init(with config: Config)
    func start(with parameters: Parameters)
}
