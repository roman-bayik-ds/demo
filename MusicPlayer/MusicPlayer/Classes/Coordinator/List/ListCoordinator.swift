//
//  ListCoordinator.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - ListCoordinator
final class ListCoordinator: AbstractCoordinator, CoordinatorProtocol {

    typealias Config = UINavigationController
    typealias Parameters = Void

    // MARK: - Public methods
    
    func start(with parameters: Parameters = ()) {
		
        let viewController = ViewControllersFactory.listViewController(paramaters: parameters, coordinator:  self)
        navigation.setViewControllers([viewController], animated: true)
    }
}

// MARK: - PlayerCoordinatorProtocol
extension ListCoordinator: ListCoordinatorProtocol {
    
	func startPlayerCoordinator() {
       
		removeChild(self)
        let playerCoordinator = PlayerCoordinator(with: navigation)
        addChild(playerCoordinator)
        playerCoordinator.start()
    }
}
