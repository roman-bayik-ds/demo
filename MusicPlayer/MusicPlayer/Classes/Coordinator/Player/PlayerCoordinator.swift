//
//  PlayerCoordinator.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - PlayerCoordinator
final class PlayerCoordinator: AbstractCoordinator, CoordinatorProtocol {

    typealias Config = UINavigationController
    typealias Parameters = Void

    // MARK: - Public methods
    
    func start(with parameters: Parameters = ()) {
		
        navigation.isNavigationBarHidden = true
        let viewController = ViewControllersFactory.playerViewController(parameters: (), coordinator: self)
        navigation.setViewControllers([viewController], animated: true)
    }
}

// MARK: - PlayerCoordinatorProtocol

extension PlayerCoordinator: PlayerCoordinatorProtocol {
    
    func startListCoordinator() {
		
        removeChild(self)
        let listCoordinator = ListCoordinator(with: navigation)
        addChild(listCoordinator)
        listCoordinator.start()
    }
}
