//
//  ControllersFactory.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - ViewControllersFactory
enum ViewControllersFactory {
    
	// MARK: - Public methods
	
    static func playerViewController(parameters: PlayerPresenter.Parameters,
                                     coordinator: PlayerPresenter.Coordinator) -> PlayerViewController {
        
        let presenter = PlayerPresenter(parameters: parameters, coordinator: coordinator)
        let viewController: PlayerViewController = UIStoryboard(.player).instantiateViewController()
        
        viewController.presenter = presenter
        presenter.view = viewController
        
        return viewController
    }
    
    static func listViewController(paramaters: ListPresenter.Parameters,
                                   coordinator: ListPresenter.Coordinator) -> ListViewController {
        
        let presenter = ListPresenter(parameters: paramaters, coordinator: coordinator)
        let viewController: ListViewController = UIStoryboard(.list).instantiateViewController()
        
        viewController.presenter = presenter
        presenter.view = viewController
        
        return viewController
    }
}
