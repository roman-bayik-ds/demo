//
//  Storyboard.swift
//  MusicPlayer
//
//  Created by DS on 13.05.2021.
//  Copyright © 2021 DS. All rights reserved.
//

import Foundation

// MARK: - Storyboard
enum Storyboard: String {
   
	case player
	case list
	
	var name: String {
		return rawValue.capitalized
	}
}
