//
//  UICollectionView+Extensions.swift
//  MusicPlayer
//
//  Created by DS on 25.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - UICollectionView
extension UICollectionView {
    
    func registerCell<T: UICollectionViewCell>(cellClass: T.Type) {
        
        register(cellClass, forCellWithReuseIdentifier: T.indentifier)
    }

    func registerCell<T: UICollectionViewCell>(xib: T.Type) {
        
        let nib = UINib(nibName: T.indentifier, bundle: nil)
        register(nib, forCellWithReuseIdentifier: T.indentifier)
    }

    func instiateCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T {
        
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.indentifier, for: indexPath) as? T else {
            fatalError("Couldn't instantiate cell with identifier \(T.indentifier)")
        }
        
        return cell
    }
}
