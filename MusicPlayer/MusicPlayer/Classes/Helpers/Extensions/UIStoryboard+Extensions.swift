//
//  UIStoryboard+Extensions.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - UIStoryboard
extension UIStoryboard {
    
    convenience init(_ storyboard: Storyboard) {
        
        self.init(name: storyboard.name, bundle: .main)
    }
    
    func instantiateViewController<T: UIViewController>() -> T {
        guard let viewController = instantiateViewController(withIdentifier: T.indentifier) as? T else {
            fatalError("Couldn't instantiate view controller with identifier \(T.indentifier)")
        }
        
        return viewController
    }
}
