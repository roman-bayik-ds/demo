//
//  UIView+Extensions.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - Indentifier
extension UIView {
    
    static var indentifier: String {
        return String(describing: self)
    }
}

// MARK: - Shadow
extension UIView {
        
    func addShadows(cornerRadius: CGFloat,
                    darkShadowColor: UIColor? = UIColor.gray.withAlphaComponent(0.5),
                    darkShadowRadius: CGFloat = 10,
                    darkShadowOpacity: Float = 1,
                    darkShadowOffset: CGSize = CGSize(width: 10, height: 10),
                    lightShadowColor: UIColor? = UIColor.white,
                    lightShadowRadius: CGFloat = 10,
                    lightShadowOpacity: Float = 1,
                    lightShadowOffset: CGSize = CGSize(width: -10, height: -10),
                    backgroundColor: UIColor? = UIColor(named: "app_color")) {
        
        if self is UIButton {
            fatalError("restricted to use this method addShadows on UIButton")
        }
        
        // dark shadow
        layer.masksToBounds = false
        layer.cornerRadius = cornerRadius
        layer.shadowRadius = darkShadowRadius
        layer.shadowOpacity = darkShadowOpacity
        layer.shadowOffset = darkShadowOffset
        layer.shadowColor = darkShadowColor?.cgColor
        
        // light shadow
        let shadowLayer = CAShapeLayer()
        shadowLayer.frame = bounds
        shadowLayer.cornerRadius = cornerRadius
        shadowLayer.backgroundColor = backgroundColor?.cgColor
        shadowLayer.shadowColor = lightShadowColor?.cgColor
        shadowLayer.shadowRadius = lightShadowRadius
        shadowLayer.shadowOpacity = lightShadowOpacity
        shadowLayer.shadowOffset = lightShadowOffset
        
        layer.insertSublayer(shadowLayer, at: 0)
    }
    
    func removeShadows() {
		
        layer.shadowOpacity = 0
        layer.sublayers?.forEach({ sublayer in
            
			if let shadowLayer = sublayer as? CAShapeLayer {
                shadowLayer.removeFromSuperlayer()
            }
        })
    }
    
    func roundByHeight() {
        layer.cornerRadius = frame.height / 2
    }
    
    func roundByWidth() {
        layer.cornerRadius = frame.width / 2
    }
	
	func loadViewFromNib(name: String?) -> UIView {
		
		let xibName = name ?? Self.identifier
		let bundle = Bundle(for: Self.self)
		
		guard let view = bundle.loadNibNamed(xibName, owner: self, options: nil)?.first as? UIView else {
			fatalError()
		}
		
		return view
	}
}

// MARK: - Identifiable
extension UIView: Identifiable {
	
	static var identifier: String {
		return String(describing: self)
	}
}
