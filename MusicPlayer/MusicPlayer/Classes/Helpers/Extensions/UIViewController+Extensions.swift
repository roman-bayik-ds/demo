//
//  UIViewController+Extensions.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - Indentifier
extension UIViewController {
    
    static var indentifier: String {
        return String(describing: self)
    }
}
