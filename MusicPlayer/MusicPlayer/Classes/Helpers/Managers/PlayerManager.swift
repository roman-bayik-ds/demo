//
//  PlayerManager.swift
//  MusicPlayer
//
//  Created by DS on 26.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import Foundation
import AVFoundation

// MARK: - PlayerManagerDelegate
protocol PlayerManagerDelegate: AnyObject {
	
    func progressWasChanged(progress value: Float, currentTime: TimeInterval)
    func onAudioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    func onAudioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?)
}

// MARK: - PlayerManager
class PlayerManager: NSObject {
    
    // MARK: - Properties
    
    static let shared = PlayerManager()
    weak var delegate: PlayerManagerDelegate?
    
    private var player: AVAudioPlayer?
    private var updateTimer: Timer?
    
    var currentSong: Song?
    var isPlaying = false
        
    // MARK: - Initialization
    
    private override init() {
        super.init()
    }
    
    // MARK: - Public methods
    
    func playSong(_ song: Song) {
        
		if player?.url == song.url {
           
			play()
            isPlaying = true
            return
        }
        do {
           
			player = try AVAudioPlayer(contentsOf: song.url)
            player?.delegate = self
            
            currentSong = song
            player?.play()
            isPlaying = true
            
            startTimer()
            UserPreferences.lastPlayedSongFileName = song.url.lastPathComponent
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func pause() {
		
        guard let player = player else { return }
        player.pause()
        isPlaying = false
    }
    
    func play() {
		
        guard let player = player else { return }
        player.play()
		isPlaying = true
    }
    
    func changeProgress(_ progress: Double) {
		
        guard let player = player else {
            return
        }
        
        let newCurrentTime = player.duration * progress
        player.currentTime = newCurrentTime
        updateProgress()
    }
    
    // MARK: - Private methods
    
    private func startTimer() {
        updateTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateProgress), userInfo: nil, repeats: true)
    }
    
    @objc private func updateProgress() {
        
        guard let player = player else { return }
        
        let progress = Float(player.currentTime / player.duration)
        
        delegate?.progressWasChanged(progress: progress, currentTime: player.currentTime)
    }
}

// MARK: - AVAudioPlayerDelegate
extension PlayerManager: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
		isPlaying = false
        delegate?.onAudioPlayerDidFinishPlaying(player, successfully: flag)
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        delegate?.onAudioPlayerDecodeErrorDidOccur(player, error: error)
    }
}
