//
//  SongsManager.swift
//  MusicPlayer
//
//  Created by DS on 26.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import Foundation
import AVFoundation
import MediaPlayer

// MARK: - SongsManager
class SongsManager {
    
    // MARK: - Properties
    
    static let shared = SongsManager()
    var songs: [Song] {
        getSongs()
    }
    var lastPlayedSong: Song? {
        
		get {
            
			guard let lastPlayedSongFileName = UserPreferences.lastPlayedSongFileName else {
                return songs.first
            }
			
            return songs.first(where: { $0.url.lastPathComponent == lastPlayedSongFileName })
        }
        set {
            UserPreferences.lastPlayedSongFileName = newValue?.url.lastPathComponent
        }
    }
    
    // MARK: - Init methods
    
    private init() { }
    
    // MARK: - Private methods
    
    private func getSongs() -> [Song] {
        
		guard let urls = Bundle.main.urls(forResourcesWithExtension: Constants.extensionType, subdirectory: Constants.subdirectoryString) else { return []
		}
        
        var songs = [Song]()
        
        urls.forEach { url in
			
            let song = getSong(at: url)
            songs.append(song)
        }
        
        return songs
    }
        
    private func getSong(at url: URL) -> Song {
       
        let playerItem = AVPlayerItem(url: url)

        let song = Song(url: url, duration: playerItem.asset.duration.seconds)
        
        let metadata = playerItem.asset.metadata
       
        metadata.forEach { (item) in
			
            guard let key = item.commonKey else { return }
            
            switch key {
			
            case .commonKeyArtist:
                song.artist = item.value as? String
            case .commonKeyTitle:
                song.title = item.value as? String
            case .commonKeyAlbumName:
                song.albumName = item.value as? String
            case .commonKeyArtwork:
                song.image = UIImage(data: item.value as! Data)
            default:
                break
            }
        } 
        
        return song
    }
}

// MARK: - Constants
private extension SongsManager {
	
	enum Constants {
		
		static let subdirectoryString: String = "Songs"
		static let extensionType: String = ".mp3"
	}
}
