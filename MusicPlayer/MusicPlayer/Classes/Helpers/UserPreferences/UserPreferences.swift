//
//  UserPreferences.swift
//  MusicPlayer
//
//  Created by DS on 26.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import Foundation

// MARK: - UserPreferences
class UserPreferences {
    
    // MARK: - Keys
    
    private enum Keys {
        static let lastPlayedSongFileNameKey = "LastPlayedSongFileName"
    }
    
    // MARK: - Properties
        
    static var lastPlayedSongFileName: String? {
        get {
			
            guard let fileName = UserDefaults.standard.value(forKey: Keys.lastPlayedSongFileNameKey) as? String else {
                return nil
            }
            
            return fileName
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.lastPlayedSongFileNameKey)
        }
    }
    
    // MARK: - Initialization
    
    private init() { }
}
