//
//  Song.swift
//  MusicPlayer
//
//  Created by DS on 26.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit
import AVFoundation

// MARK: - Song
class Song {
    
	// MARK: - Properties
	
    var title: String? = nil
    var artist: String? = nil
    var albumName: String? = nil
    var image: UIImage? = nil
    let duration: Double
    let url: URL
    
	// MARK: - Initialization
	
    init(url: URL, duration: Double) {
       
		self.url = url
        self.duration = duration
    }
}

// MARK: - Equatable
extension Song: Equatable {
	
    static func == (lhs: Song, rhs: Song) -> Bool {
        return lhs.url == rhs.url
    }
}
