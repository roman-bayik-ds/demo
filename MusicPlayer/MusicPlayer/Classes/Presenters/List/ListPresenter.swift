//
//  ListPresenter.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - ListPresenter
final class ListPresenter {
    
    // MARK: - Constants
    
    private enum Constants {
		
        static let itemHeight: CGFloat = 81
        static let emptySpaceToEdges: CGFloat = 32
        static let sizeForHeader = CGSize(width: 0, height: 4)
    }
    
    // MARK: - Properties
    
    unowned var view: View!
    weak var coordinator: Coordinator!
    private let parameters: Parameters
    private var selectedIndex: Int?
    
    private let songs = SongsManager.shared.songs
    
    // MARK: - Init methods
    
    required init(parameters: Parameters, coordinator: Coordinator) {
       
		self.parameters = parameters
        self.coordinator = coordinator
        
        if let currentSong = PlayerManager.shared.currentSong,
		   let index = songs.firstIndex(where: {$0 == currentSong}) {
               selectedIndex = index
        }
    }
}

// MARK: - PresenterProtocol
extension ListPresenter: PresenterProtocol {
	
	typealias View = ListViewProtocol
	typealias Coordinator = ListCoordinatorProtocol
	typealias Parameters = Void
}

// MARK: - ListPresenterProtocol
extension ListPresenter: ListPresenterProtocol {
	
	func onLikeButtonClicked() {
		// TO DO:
	}
	
	func onSettingsButtonClicked() {
		// TO DO:
	}
	
    func onViewDidLoad() {
		
        guard let selectedIndex = selectedIndex else { return }
        view.updateSongImageView(image: songs[selectedIndex].image)
    }
    
    var numberOfItems: Int {
        return songs.count
    }
    
    var sizeForHeader: CGSize {
        Constants.sizeForHeader
    }
    
    func sizeForItem(at indexPath: IndexPath, in collectionView: UICollectionView) -> CGSize {
		
        let width = collectionView.frame.width - Constants.emptySpaceToEdges
        return CGSize(width: width, height: Constants.itemHeight)
    }
    
    func configureCell(_ cell: SongCollectionViewCell, at indexPath: IndexPath) {
       
		if selectedIndex == indexPath.row {
			
            cell.setSelected()
            cell.isPlaying = PlayerManager.shared.isPlaying
        } else {
            cell.setNormal()
        }
		
        cell.set(title: songs[indexPath.row].title, artist: songs[indexPath.row].artist)
    }
    
    func didSelectItem(at indexPath: IndexPath) {

        guard selectedIndex != indexPath.row else {
			
            coordinator.startPlayerCoordinator()
            return
        }
        
        let previousSelectedIndex = selectedIndex
        selectedIndex = indexPath.row
        
        let song = songs[indexPath.row]
        PlayerManager.shared.playSong(song)
        
        view.updateSongImageView(image: song.image)
        
        if let previousSelectedIndex = previousSelectedIndex {
            view.reloadCollectionViewItems(at: [indexPath, IndexPath(row: previousSelectedIndex, section: 0)])
        } else {
            view.reloadCollectionViewItems(at: [indexPath])
        }
    }
    
    func didPressPlayButton(at cell: SongCollectionViewCell, with indexPath: IndexPath) {
       
		if selectedIndex == indexPath.row {
			
            if cell.isPlaying {
				
                cell.isPlaying = false
                PlayerManager.shared.pause()
            } else {
				
                cell.isPlaying = true
                PlayerManager.shared.play()
            }
        } else {
            didSelectItem(at: indexPath)
        }
    }
}
