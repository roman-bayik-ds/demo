//
//  PlayerPresenter.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit
import AVFoundation

// MARK: - PlayerPresenter
final class PlayerPresenter {
    
    // MARK: - Constants
    
    private enum Constants {
		
        static let pauseImage = UIImage(systemName: "pause.fill")
        static let playImage = UIImage(systemName: "play.fill")
		static let dateFormat: String = "mm:ss"
    }
    
    // MARK: - Properties
    
    unowned var view: View!
    weak var coordinator: Coordinator!
    private let parameters: Parameters
    private var currentSong: Song?
    private var songs: [Song]
    private var dateFormatter: DateFormatter {
		
        let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = Constants.dateFormat
        return dateFormatter
    }
    
    // MARK: - Init methods
    
    required init(parameters: Parameters, coordinator: PlayerCoordinatorProtocol) {
       
		self.parameters = parameters
        self.coordinator = coordinator
        currentSong = SongsManager.shared.lastPlayedSong
        songs = SongsManager.shared.songs
        PlayerManager.shared.delegate = self
    }
    
    // MARK: - Private methods
    
    private func updateView(with song: Song) {
       
		let date = Date(timeIntervalSinceReferenceDate: song.duration)
        
        let buttonImage = PlayerManager.shared.isPlaying ? Constants.pauseImage : Constants.playImage
        
        var isForwardButtonEnabled = false
        var isBackwardButtonEnabled = false
        
        if let index = songs.firstIndex(of: song) {
			
            isForwardButtonEnabled = index + 1 < songs.count
            isBackwardButtonEnabled = index > 0
        }

        view.set(songImage: song.image,
                 title: song.title,
                 artist: song.artist,
                 duration: dateFormatter.string(from: date),
                 isSelectedPlayButton: PlayerManager.shared.isPlaying,
                 playButtonImage: buttonImage,
                 isForwardButtonEnabled: isForwardButtonEnabled,
                 isBackwardButtonEnabled: isBackwardButtonEnabled)
    }
    
    private func playNextSong() {
       
        guard let currentSong = currentSong, let index = songs.firstIndex(of: currentSong), index + 1 < songs.count else {
			
            updateView(with: self.currentSong!)
            return
        }
		
        let nextSong = songs[index + 1]
        
        PlayerManager.shared.playSong(nextSong)
        self.currentSong = nextSong
        
        updateView(with: nextSong)
    }
    
    private func playPreviousSong() {
        
        guard let currentSong = currentSong, let index = songs.firstIndex(of: currentSong) else { return }
        let previousSong = songs[index - 1]
        
        PlayerManager.shared.playSong(previousSong)
        self.currentSong = previousSong
        
        updateView(with: previousSong)
    }
}

// MARK: - PlayerPresenterProtocol

extension PlayerPresenter: PresenterProtocol, PlayerPresenterProtocol {
    
    typealias View = PlayerViewProtocol
    typealias Coordinator = PlayerCoordinatorProtocol
    typealias Parameters = Void
    
    func onViewDidLoad() {
        
        guard let currentSong = currentSong else { return }
   
        updateView(with: currentSong)
    }
    
    func onListButtonPressed() {
        coordinator.startListCoordinator()
    }
    
    func onForwardTouchedUp() {
        playNextSong()
    }
    
    func onBackwardButtonTouchedUp() {
        playPreviousSong()
    }
    
    func onPlayButtonTouchedUp() {
        
        guard let currentSong = currentSong else { return }
        
        if PlayerManager.shared.isPlaying {
            PlayerManager.shared.pause()
        } else {
            PlayerManager.shared.playSong(currentSong)
        }
        
        let buttonImage = PlayerManager.shared.isPlaying ? Constants.pauseImage : Constants.playImage
		
		view?.setPlayButtonUI(image: buttonImage, isPlaying: PlayerManager.shared.isPlaying)
    }
    
    func onSliderValueChanged(_ value: CGFloat) {
        PlayerManager.shared.changeProgress(Double(value))
    }
    
    func onButtonTouchedDown(_ button: AppButton) {
        button.isSelected = true
    }
    
    func onButtonDraggedOutside(_ button: AppButton) {
        button.isSelected = false
    }
}

// MARK: - PlayerManagerDelegate

extension PlayerPresenter: PlayerManagerDelegate {
    
    func progressWasChanged(progress value: Float, currentTime: TimeInterval) {
        
        let date = Date(timeIntervalSinceReferenceDate: currentTime)
        view.updatePlayerCurrentTime(currentTime: dateFormatter.string(from: date))
        
        view.updateSliderProgress(value: value)
    }
    
    func onAudioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        playNextSong()
    }
    
    func onAudioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
      
		if let error = error {
            print(error.localizedDescription)
        }
    }
}
