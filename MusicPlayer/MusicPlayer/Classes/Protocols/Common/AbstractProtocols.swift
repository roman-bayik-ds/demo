//
//  AbstractProtocols.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import Foundation

// MARK: - PresenterProtocol

protocol PresenterProtocol {

    associatedtype View
    associatedtype Coordinator
    associatedtype Parameters
	
    var view: View! { get set }
    var coordinator: Coordinator! { get set }

    init(parameters: Parameters, coordinator: Coordinator)
}

// MARK: - ViewProtocol

protocol ViewProtocol: AnyObject {

    associatedtype Presenter
    var presenter: Presenter! { get set }
}
