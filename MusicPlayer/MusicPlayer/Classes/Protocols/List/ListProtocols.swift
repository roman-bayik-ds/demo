//
//  ListProtocols.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - ListViewProtocol
protocol ListViewProtocol: AnyObject {
	
    func reloadCollectionViewItems(at indexPaths: [IndexPath])
    func updateSongImageView(image: UIImage?)
}

// MARK: - ListPresenterProtocol
protocol ListPresenterProtocol {
    
    var numberOfItems: Int { get }
    var sizeForHeader: CGSize { get }
    
    func onViewDidLoad()
    func sizeForItem(at indexPath: IndexPath, in collectionView: UICollectionView) -> CGSize
    func configureCell(_ cell: SongCollectionViewCell, at indexPath: IndexPath)
    func didSelectItem(at indexPath: IndexPath)
    func didPressPlayButton(at cell: SongCollectionViewCell, with indexPath: IndexPath)
    func onLikeButtonClicked()
	func onSettingsButtonClicked()
}

// MARK: - ListCoordinatorProtocol

protocol ListCoordinatorProtocol: AnyObject {
    func startPlayerCoordinator()
}
