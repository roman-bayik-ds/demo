//
//  PlayerProtocols.swift
//  MusicPlayer
//
//  Created by DS on 24.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - PlayerViewProtocol

protocol PlayerViewProtocol: AnyObject {

    func set(songImage: UIImage?,
             title: String?,
             artist: String?,
             duration: String?,
             isSelectedPlayButton: Bool,
             playButtonImage: UIImage?,
             isForwardButtonEnabled: Bool,
             isBackwardButtonEnabled: Bool)
    
    func updateSliderProgress(value: Float)
    func updatePlayerCurrentTime(currentTime: String?)
	func setPlayButtonUI(image: UIImage?, isPlaying: Bool)
}

// MARK: - PlayerPresenterProtocol

protocol PlayerPresenterProtocol {

    func onViewDidLoad()
    func onListButtonPressed()
    func onForwardTouchedUp()
    func onBackwardButtonTouchedUp()
    func onPlayButtonTouchedUp()
    func onSliderValueChanged(_ value: CGFloat)
    func onButtonTouchedDown(_ button: AppButton)
    func onButtonDraggedOutside(_ button: AppButton)
}

// MARK: - PlayerCoordinatorProtocol

protocol PlayerCoordinatorProtocol: AnyObject {
    func startListCoordinator()
}
