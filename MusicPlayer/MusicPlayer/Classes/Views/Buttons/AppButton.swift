//
//  AppButton.swift
//  MusicPlayer
//
//  Created by DS on 25.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - AppButton
class AppButton: UIButton {
    
    // MARK: - Constants
    
    private enum Constants {
		
        static let shadowRadius: CGFloat = 1
        static let darkShadowOpacity: Float = 0.5
        static let darkShadowOffset = CGSize(width: 1, height: 1)
        static let lightShadowOpacity: Float = 1
        static let lightShadowOffset = CGSize(width: -1, height: -1)
        static let appColor = UIColor(named: "app_color")
        static let selectedColor = UIColor(named: "selected_color")
        static let fontColor = UIColor(named: "default_font_color")
        static let borderSelectedColor = UIColor(named: "border_selected_color")
    }
    
    // MARK: - Properties
    
    private let shadowLayer = CAShapeLayer()
    private var touchPath: UIBezierPath {
        return UIBezierPath(ovalIn: bounds)
    }
    
    override var isSelected: Bool {
        didSet {
			
            tintColor = isSelected ? .white : Constants.fontColor
            shadowLayer.backgroundColor = isSelected ? Constants.selectedColor?.cgColor : Constants.appColor?.cgColor
            layer.shadowOffset = isSelected ? Constants.lightShadowOffset : Constants.darkShadowOffset
            shadowLayer.shadowOffset = isSelected ? Constants.darkShadowOffset : Constants.lightShadowOffset
        }
    }
    
//    override var isHighlighted: Bool {
//        didSet {
//            tintColor = isHighlighted ? .white : Constants.fontColor
//            shadowLayer.backgroundColor = isHighlighted ? Constants.selectedColor?.cgColor : Constants.appColor?.cgColor
//            layer.shadowOffset = isHighlighted ? Constants.lightShadowOffset : Constants.darkShadowOffset
//            shadowLayer.shadowOffset = isHighlighted ? Constants.darkShadowOffset : Constants.lightShadowOffset
//        }
//    }
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = Constants.appColor
        tintColor = Constants.fontColor
        adjustsImageWhenHighlighted = false
        
        setupShadows()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        
//		layer.borderWidth = 4
//		layer.borderColor = #colorLiteral(red: 0.8784313725, green: 0.9254901961, blue: 0.9960784314, alpha: 1)
        layer.cornerRadius = frame.width / 2
        shadowLayer.frame = bounds
        shadowLayer.cornerRadius = bounds.width / 2
    }
    
    // MARK: - Override methods
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return touchPath.contains(point)
    }
    
    // MARK: - Private methods
    
    private func setupShadows() {
        
        // dark shadow
        layer.shadowRadius = Constants.shadowRadius
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = Constants.darkShadowOpacity
        layer.shadowOffset = Constants.darkShadowOffset
        
        // light shadow
        shadowLayer.backgroundColor = Constants.appColor?.cgColor
        shadowLayer.shadowColor = UIColor.white.cgColor
        shadowLayer.shadowOffset = Constants.lightShadowOffset
        shadowLayer.shadowOpacity = Constants.lightShadowOpacity
        shadowLayer.shadowRadius = Constants.shadowRadius
        
        layer.insertSublayer(shadowLayer, below: imageView?.layer)
    }
}
