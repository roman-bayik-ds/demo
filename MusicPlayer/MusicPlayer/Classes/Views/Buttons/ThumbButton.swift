//
//  ThumbButton.swift
//  MusicPlayer
//
//  Created by DS on 03.04.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - ThumbButton
class ThumbButton: UIButton {
	
    // MARK: - Properties
	
	private var isLayoutSubviewsCalled: Bool = false
	
    // MARK: - Constants
    
    private enum Constants {
		
        static let darkShadowRadius: CGFloat = 4
        static let darkShadowOpacity: Float = 0.5
        static let darkShadowOffset = CGSize(width: -4, height: -4)
        static let lightShadowRadius: CGFloat = 4
        static let lightShadowOpacity: Float = 1
        static let lightShadowOffset = CGSize(width: 4, height: 4)
        static let backgroundColor = UIColor(named: "selected_color")
    }
    
	// MARK: - Private methods
	
	private func setupUI() {
		
		roundByWidth()
		
		guard let imageView = imageView  else {
			return
		}
		
		imageView.addShadows(cornerRadius: imageView.frame.height / 2,
							 darkShadowColor: UIColor.gray,
							 darkShadowRadius: Constants.darkShadowRadius,
							 darkShadowOpacity: Constants.darkShadowOpacity,
							 darkShadowOffset: Constants.darkShadowOffset,
							 lightShadowRadius: Constants.lightShadowRadius,
							 lightShadowOpacity: Constants.lightShadowOpacity,
							 lightShadowOffset: Constants.lightShadowOffset,
							 backgroundColor: Constants.backgroundColor)
	}
	
	// MARK: - Lifecycle
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		if !isLayoutSubviewsCalled {
			
			setupUI()
			isLayoutSubviewsCalled = true
		}
	}
}
