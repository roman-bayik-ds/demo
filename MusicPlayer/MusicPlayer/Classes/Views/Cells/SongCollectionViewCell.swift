//
//  SongCollectionViewCell.swift
//  MusicPlayer
//
//  Created by DS on 25.03.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - SongCollectionViewCellDelegate
protocol SongCollectionViewCellDelegate: AnyObject {
    func didPressPlayButton(at cell: SongCollectionViewCell)
}

// MARK: - SongCollectionViewCell
class SongCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Constants
    
    private enum Constants {
		
        static let cornerRadius: CGFloat = 10
        static let shadowRadius: CGFloat = 1
        static let darkShadowOffset = CGSize(width: 0, height: -1)
        static let lightShadowOffset = CGSize(width: 0, height: 1)
        static let selectedColor = UIColor(named: "border_color")
        static let pauseImage = UIImage(systemName: "pause.fill")
        static let playImage = UIImage(systemName: "play.fill")
    }
    
    // MARK: - Properties
    
    weak var delegate: SongCollectionViewCellDelegate?
    var isPlaying: Bool = false {
        didSet {
			
			let image = isPlaying ? Constants.pauseImage : Constants.playImage
			playButtonView.setButtonIsSelected(isPlaying)
			playButtonView.setButtonImage(image)
			playButtonView.changeShadowOffsets(isPlaying)
//            playButton.isSelected = isPlaying
//
//            playButton.setImage(image, for: .normal)
        }
    }
    
    // MARK: - Outlets
    
	@IBOutlet weak var playButtonView: AppButtonView! {
		didSet {
			
			playButtonView.setType(type: .play)
			playButtonView.delegate = self
		}
	}
	
	@IBOutlet private weak var titleLabel: UILabel!
	@IBOutlet private weak var artistLabel: UILabel!
    
    // MARK: - Actions
    
//    @IBAction private func playButtonAction(_ sender: AppButton) {
//        delegate?.didPressPlayButton(at: self)
//    }
//
//    @IBAction private func buttonTouchedDown(_ sender: AppButton) {
//        sender.isSelected = true
//    }
//
//    @IBAction private func buttonDraggedOutside(_ sender: AppButton) {
//        sender.isSelected = false
//    }
    
    // MARK: - Public methods
    
    func set(title: String?, artist: String?) {
        
        titleLabel.text = title
        artistLabel.text = artist
    }
    
    func setNormal() {
		
        isPlaying = false
        backgroundColor = .clear
        removeShadows()
    }
    
    func setSelected() {
		
        isPlaying = true
        addShadows(cornerRadius: Constants.cornerRadius,
                   darkShadowRadius: Constants.shadowRadius,
                   darkShadowOffset: Constants.darkShadowOffset,
                   lightShadowRadius: Constants.shadowRadius,
                   lightShadowOffset: Constants.lightShadowOffset,
                   backgroundColor: Constants.selectedColor)
    }
}

// MARK: - AppButtonViewDelegate
extension SongCollectionViewCell: AppButtonViewDelegate {
	
	func appButtonTouchedUp(_ purpleButtonView: AppButtonView, type: AppButtonType) {
		
		switch type {
		
		case .play:
			delegate?.didPressPlayButton(at: self)
		default:
			break
		}
	}
}
