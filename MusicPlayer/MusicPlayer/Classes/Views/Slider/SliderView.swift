//
//  SliderView.swift
//  MusicPlayer
//
//  Created by DS on 03.04.2020.
//  Copyright © 2020 DS. All rights reserved.
//

import UIKit

// MARK: - SliderViewDelegate
protocol SliderViewDelegate: AnyObject {
    func sliderViewValueDidChanged(_ value: CGFloat)
}

// MARK: - SliderView
class SliderView: UIView {
    
    // MARK: - Constants
    
    private enum Constants {
        
		static let shadowRadius: CGFloat = 1
        static let darkShadowOffset = CGSize(width: 0, height: -1)
        static let lightShadowOffset = CGSize(width: 0, height: 1)
        static let selectedColor = UIColor(named: "selected_color")
    }
    
    // MARK: - Properties
    
    weak var delegate: SliderViewDelegate?
	private var isLayoutSubviewsCalled = false
    private var isThumbMoving = false
    private var previousCenter = CGPoint.zero
    private var minimumValue: CGFloat = 0
    private var maximumValue: CGFloat = 1
    
	var value: CGFloat = 0 {
        didSet {
			
            let position = positionForValue(value)
            UIView.animate(withDuration: 0.1) {
                //self.thumbButton.center = CGPoint(x: position, y: self.thumbButton.center.y)
				self.setWidthForProgressView(position)
            }
        }
    }
    
    // MARK: - Outlets
    
	@IBOutlet private weak var thumbButtonLeadingConstraint: NSLayoutConstraint!
	@IBOutlet private weak var widthProgressViewConstraint: NSLayoutConstraint!
    @IBOutlet private weak var progressView: UIView!
    @IBOutlet private weak var thumbButton: ThumbButton!
    
    // MARK: - Lifecycle
    
	override func layoutSubviews() {
		super.layoutSubviews()
		
		if !isLayoutSubviewsCalled {
			
			thumbButtonLeadingConstraint.constant = -(thumbButton.frame.height / 2)
			setupProgressViewUI()
			isLayoutSubviewsCalled = true
		}
	}
	
    // MARK: - Actions
    
    @IBAction private func thumbDidPan(_ sender: UIPanGestureRecognizer) {
        
        let translation = sender.translation(in: self)

        switch sender.state {
        case .began:
            
            previousCenter = thumbButton.center
            isThumbMoving = true
        case .changed:
			
            value = (previousCenter.x + translation.x) / bounds.width
            value = boundValue(value)
        case .ended:
			
            isThumbMoving = false
            delegate?.sliderViewValueDidChanged(value)
        default:
            break
        }
    }
    
    @IBAction private func thumbButtonTouchedDown(_ sender: UIButton) {
        
//        isThumbMoving = true
    }
    
    // MARK: - Public methods
    
    func setProgress(value: CGFloat) {
		
        guard !isThumbMoving else { return }
        self.value = value
    }
    
    // MARK: - Private methods
	private func setupProgressViewUI() {
		
		progressView.backgroundColor = Constants.selectedColor
		progressView.roundByHeight()

		addShadows(cornerRadius: frame.height / 2,
				   darkShadowRadius: Constants.shadowRadius,
				   darkShadowOffset: Constants.darkShadowOffset,
				   lightShadowRadius: Constants.shadowRadius,
				   lightShadowOffset: Constants.lightShadowOffset,
				   backgroundColor: UIColor.gray.withAlphaComponent(0.1))
	}
	
	private func setWidthForProgressView(_ value: CGFloat) {
		widthProgressViewConstraint.constant = value
	}
	
    private func positionForValue(_ value: CGFloat) -> CGFloat {
        return bounds.width * value
    }
    
    private func thumbOriginForValue(_ value: CGFloat) -> CGPoint {
		
        let x = positionForValue(value) - thumbButton.frame.width / 2.0
        return CGPoint(x: x, y: thumbButton.center.y)
    }
    
    private func boundValue(_ value: CGFloat) -> CGFloat {
        return min(max(value, minimumValue), maximumValue)
    }
}
