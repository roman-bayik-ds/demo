//
//  XibView.swift
//  MusicPlayer
//
//  Created by DS on 14.05.2021.
//  Copyright © 2021 DS. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class XibView : UIView {
	
	// MARK: - Properties
	
	var contentView: UIView?
	
	@IBInspectable var nibName: String?
	
	// MARK: - Initialization
	
	override init(frame: CGRect) {
		super.init(frame: frame)

		commonInit()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)

		commonInit()
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		setup()
	}
	
	override func prepareForInterfaceBuilder() {
		super.prepareForInterfaceBuilder()
		
		commonInit()
		setup()
		contentView?.prepareForInterfaceBuilder()
	}
	
	// MARK: - Public functions
	
	 func setup() {}
	
	// MARK: - Private methods
	
	private func commonInit() {
		
		let contentView = loadViewFromNib(name: nibName)
		
		contentView.frame = bounds
		contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		addSubview(contentView)
		
		self.contentView = contentView
	}
}
