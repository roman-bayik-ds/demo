//
//  AppButtonType.swift
//  MusicPlayer
//
//  Created by DS on 14.05.2021.
//  Copyright © 2021 DS. All rights reserved.
//

import UIKit

// MARK: - AppButtonType
enum AppButtonType {
	
	// MARK: - Cases
	
	case backward
	case forward
	case play
	case like
	case list
	case settings
	
	// MARK: - Properties
	
	var image: UIImage? {
		
		switch self {
		
		case .backward:
			return UIImage(systemName: "backward.fill")
		case .forward:
			return UIImage(systemName: "forward.fill")
		case .play:
			return UIImage(systemName: "play.fill")
		case .like:
			return UIImage(systemName: "heart.fill")
		case .list:
			return UIImage(systemName: "line.horizontal.3")
		case .settings:
			return UIImage(systemName: "ellipsis")
		}
	}
}
