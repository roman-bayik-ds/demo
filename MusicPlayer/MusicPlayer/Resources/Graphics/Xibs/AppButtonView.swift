//
//  AppButtonView.swift
//  MusicPlayer
//
//  Created by DS on 14.05.2021.
//  Copyright © 2021 DS. All rights reserved.
//

import UIKit

// MARK: - AppButtonViewDelegate
protocol AppButtonViewDelegate: AnyObject {
	func appButtonTouchedUp(_ purpleButtonView: AppButtonView, type: AppButtonType)
}

// MARK: - AppButtonView
class AppButtonView: XibView {
	
	// MARK: - Constants
	
	private enum Constants {
		
		static let shadowRadius: CGFloat = 5
		static let darkShadowOpacity: Float = 0.5
		static let darkShadowOffset = CGSize(width: 5, height: 5)
		static let lightShadowOpacity: Float = 1
		static let lightShadowOffset = CGSize(width: -5, height: -5)
		static let appColor = UIColor(named: "app_color")
		static let selectedColor = UIColor(named: "selected_color")
		static let fontColor = UIColor(named: "default_font_color")
		static let borderSelectedColor = UIColor(named: "border_selected_color")
	}
	
	// MARK: - Properties
	
	private var type: AppButtonType = .like
	weak var delegate: AppButtonViewDelegate?
	private let shadowLayer = CAShapeLayer()
	private var touchPath: UIBezierPath {
		return UIBezierPath(ovalIn: bounds)
	}
	
	// MARK: - Outlents
	
	@IBOutlet private weak var appButton: AppButton!
	
	// MARK: - Actions
	
	@IBAction private func onAppButtonClicked(_ sender: AppButton) {
		
		appButton.isSelected = false
		changeShadowOffsets(false)
		delegate?.appButtonTouchedUp(self, type: type)
	}
	
	@IBAction private func appButtonTouchedDown(_ button: AppButton) {
		
		appButton.isSelected = true
		changeShadowOffsets(true)
	}

	@IBAction private func appButtonOutside(_ button: AppButton) {
		
		appButton.isSelected = false
		changeShadowOffsets(false)
	}
	
	// MARK: - Public methods
	
	func setType(type: AppButtonType) {
		
		self.type = type
		appButton.setImage(type.image, for: .normal)
	}
	
	func changeShadowOffsets(_ isSelected: Bool) {
		
		contentView?.backgroundColor = isSelected ? Constants.selectedColor : Constants.appColor
		shadowLayer.shadowOffset = isSelected ? Constants.darkShadowOffset : Constants.lightShadowOffset
		layer.shadowOffset = isSelected ? Constants.lightShadowOffset : Constants.darkShadowOffset
	}
	
	func setButtonIsSelected(_ isSelected: Bool) {
		appButton.isSelected = isSelected
	}
	
	func setButtonImage(_ image: UIImage?) {
		appButton.setImage(image, for: .normal)
	}
	
	func setButtonIsEnabled(_ isEnabled: Bool) {
		appButton.isEnabled = isEnabled
	}
	
	// MARK: - Private methods
	
	private func setupShadows() {
		
		// dark shadow
		layer.masksToBounds = false
		layer.shadowRadius = Constants.shadowRadius
		layer.shadowColor = UIColor.gray.cgColor
		layer.shadowOpacity = Constants.darkShadowOpacity
		layer.shadowOffset = Constants.darkShadowOffset
		
		// light shadow
		shadowLayer.backgroundColor = Constants.appColor?.cgColor
		shadowLayer.shadowColor = UIColor.white.cgColor
		shadowLayer.shadowOffset = Constants.lightShadowOffset
		shadowLayer.shadowOpacity = Constants.lightShadowOpacity
		shadowLayer.shadowRadius = Constants.shadowRadius
		
		layer.insertSublayer(shadowLayer, at: 0)
	}

	// MARK: - Initialization
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		setType(type: type)
		setupShadows()
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
	
		contentView?.roundByHeight()
		shadowLayer.frame = bounds
		shadowLayer.cornerRadius = bounds.width / 2
	}
	
	// MARK: - Override methods
	
	override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
		return touchPath.contains(point)
	}
}
